/*global logger*/
/*
 AirMilesMastercard
 ========================

 @file      : AirMilesMastercard.js
 @version   : 1.0.0
 @author    : Nils Groen
 @date      : 1/31/2017
 @copyright : Apache 2.0
 @license   : Incentro

 Documentation
 ========================
 Describe your widget here.
 */

// Required module list. Remove unnecessary modules, you can always get them back from the boilerplate.
define([
    "dojo/_base/declare",
    "mxui/widget/_WidgetBase",
    "dijit/_TemplatedMixin",

    "mxui/dom",
    "dojo/dom",
    "dojo/dom-prop",
    "dojo/dom-geometry",
    "dojo/dom-class",
    "dojo/dom-style",
    "dojo/dom-construct",
    "dojo/_base/array",
    "dojo/_base/lang",
    "dojo/text",
    "dojo/html",
    "dojo/_base/event",

    "AirMilesMastercard/lib/jquery-1.11.2",
    "AirMilesMastercard/lib/jquery.creditCardValidator",
    "AirMilesMastercard/lib/moment",
    "AirMilesMastercard/lib/jquery.payment.min",

    "dojo/text!AirMilesMastercard/widget/template/AirMilesMastercard.html"
], function (declare, _WidgetBase, _TemplatedMixin, dom, dojoDom, dojoProp, dojoGeometry, dojoClass, dojoStyle, dojoConstruct, dojoArray, dojoLang, dojoText, dojoHtml, dojoEvent, _jQuery, cardValidation, moment,payment, widgetTemplate) {
    "use strict";

    var $ = _jQuery.noConflict(true);

    // Declare widget's prototype.
    return declare("AirMilesMastercard.widget.AirMilesMastercard", [ _WidgetBase, _TemplatedMixin ], {
        // _TemplatedMixin will create our dom node using this HTML template.
        templateString: widgetTemplate,

        // DOM elements
        inputForm: null,
        inputNodes: null,
        payment_method: null,
        vault_token: null,
        // Commented out because we do not need the hidden input elements
        //setup_token: null,

        datetime_local: null,
        cvc: null,
        pan: null,
        nameCardHolder: null,
        expiry_month: null,
        expiry_year: null,
        submitButton: null,

        //Form JS
        has_errors: false,
        prevent_send: true,
        setup_token: null,


        // Parameters configured in the Modeler.
        vault_token_url: null,
        setupTokenMicroflow: "",
        saveToken_Microflow: "",
        GingerPaymentsEntity: null,
        TokenAttribute: null,
        PanHashAttribute: null,

        // Internal variables. Non-primitives created in the prototype are shared between all widget instances.
        _handles: null,
        _contextObj: null,
        _alertDiv: null,
        _readOnly: false,

        // dojo.declare.constructor is called to construct the widget instance. Implement to initialize non-primitive properties.
        constructor: function () {
            logger.debug(this.id + ".constructor");
            this._handles = [];
        },

        // dijit._WidgetBase.postCreate is called after constructing the widget. Implement to do extra setup work.
        postCreate: function () {
            logger.debug(this.id + ".postCreate");

            if (this.readOnly || this.get("disabled") || this.readonly) {
                this._readOnly = true;
            }

            this._updateRendering();
            this._setupEvents();

        },


        _getSetupToken: function (){
            // If microflow entered in mendix modeler

            // If setup_token doesn't exist
            if (this.setup_token == null){
                var self = this;
                mx.data.action({
                    params: {
                        applyto: "selection",
                        actionname: this.setupTokenMicroflow,
                        guids: [ this._contextObj.getGuid() ],
                        origin: this.mxform
                    },
                    callback: function (obj) {
                        // Save the setup token in a variable
                        self.setup_token = obj;

                    },
                    error: dojoLang.hitch(this, function (error) {
                        logger.error(this.id + ": An error occurred while executing microflow: " + error.description);
                    })
                }, this);
            }else{
                console.log('microflow did not run');
            }
        },

        // mxui.widget._WidgetBase.update is called when context is changed or initialized. Implement to re-render and / or fetch data.
        update: function (obj, callback) {
            logger.debug(this.id + ".update");

            this._contextObj = obj;

            this._getSetupToken();
            // If you want to add Initialen + Tussenvoegsel + Achternaam it should be done here.

            this._resetSubscriptions();
            this._updateRendering(callback); // We're passing the callback to updateRendering to be called after DOM-manipulation
        },

        // mxui.widget._WidgetBase.enable is called when the widget should enable editing. Implement to enable editing if widget is input widget.
        enable: function () {
            logger.debug(this.id + ".enable");
        },

        // mxui.widget._WidgetBase.enable is called when the widget should disable editing. Implement to disable editing if widget is input widget.
        disable: function () {
            logger.debug(this.id + ".disable");
        },


        // mxui.widget._WidgetBase.uninitialize is called when the widget is destroyed. Implement to do special tear-down work.
        uninitialize: function () {
            logger.debug(this.id + ".uninitialize");
            // Clean up listeners, helper objects, etc. There is no need to remove listeners added with this.connect / this.subscribe / this.own.
        },

        // We want to stop events on a mobile device
        _stopBubblingEventOnMobile: function (e) {
            logger.debug(this.id + "._stopBubblingEventOnMobile");
            if (typeof document.ontouchstart !== "undefined") {
                dojoEvent.stop(e);
            }
        },

        _display_error: function(message) {
            $('#payment-form').prepend('<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign"></span>' + message + '</div>');
            $("#payment-form :input").prop("disabled", false);
            $("#payment-form .btn-betalen").prop("disabled", false);
            $("#payment-form .btn-betalen").removeClass('btn-loader');
            this.has_errors = true;
        },

        _clear_errors: function(){
            $('#payment-form .text-error').remove();
            $("#payment-form .btn-betalen").prop("disabled", true);
            $("#payment-form .btn-betalen").addClass('btn-loader');
            this.has_errors = false;
        },

        _submitForm: function(e){
            var pan = this.pan.value;
            var holder_name= this.nameCardHolder.value;
            var expiry_month= this.expiry_month.value;
            var expiry_year= this.expiry_year.value;
            var cvc= this.cvc.value;

            if (!this.prevent_send) {
                return;
            }

            e.preventDefault();

            this._clear_errors();

            // Only accept mastercard credit card
            var pan_validation = $('#pan').validateCreditCard({ accept: ['mastercard'] });

            if (pan_validation.card_type && pan_validation.card_type.name != 'maestro') {
                if (cvc.length < 3) {
                    this._display_error('The supplied CVC/CVV is too short.');
                }

                if (cvc.length > 3) {
                    this._display_error('The supplied CVC/CVV is too long.');
                }
            } else { // for Maestro, CVC is optional, so default to 000 if it is empty
                if (cvc== '') {
                    cvc = '000'
                }
            }

            if (expiry_month.length < 1) {
                this._display_error('Month is required.');
            }
            if (expiry_year.length < 1) {
                this._display_error('Year is required.');
            }
            if (expiry_month.length > 0 && expiry_year.length > 0) {
                var expiry = moment(expiry_year + '-' + expiry_month + '-01', ['YY-MM-DD', 'YYYY-MM-DD']).endOf('month');

                if (!expiry.isValid()) {
                    this._display_error('The supplied expiration date is invalid.');
                }

                if (expiry.isValid() && expiry.isBefore(moment())) {
                    this._display_error('The supplied expiration date is in the past.');
                }
            }
            if (holder_name.length < 1) {
                this._display_error('Card holder is required.');
            }
            if (!pan_validation.length_valid) {
                this._display_error('Card number length is incorrect.');
            }
            if (!$.payment.validateCardNumber(pan)) {
                this._display_error('Card number is invalid.')
            }

            if (this.has_errors) {
                return;
            }

            // Credit card info and the setup token that we loaded with _getSetupToken()
            var pan_data = {
                "name": holder_name,
                "pan": pan.replace(/\D/g, ''),
                "expiry": expiry.format('MMYYYY'),
                "setuptoken": this.setup_token
            };

            if (cvc) {
                pan_data['cvc'] = this.cvc_Form;
            }
            // We use self in the ajax call because the scope of this changes after done. This happens because of the function(vault_token)
            var self = this;

            $.ajax({
                url: self.vault_token_url,
                data: JSON.stringify(pan_data),
                method: 'POST'
            })
                .done(function(data) {
                    // Change the pan number to pan hash
                    self.prevent_send = true;

                    // TODO: When endpoint is created watch wat data you get from Ginger Payments
                    console.log(data);
                    self.pan.value = '*************111';
                    //self.vault_token = data.vault_token;
                    //self.pan_hash = data.pan_hash;

                    self._TokenMicroflow(self);
                   // $('#payment-form').submit();
                })
                .fail(function() {
                    self._display_error('An error has occurred. Please check your card details and try again.');
                    self.vault_token = "It landed in the error flow";
                    self._TokenMicroflow(self);

                });
        },

        // Attach events to HTML dom elements
        _setupEvents: function () {
            logger.debug(this.id + "._setupEvents");

            $(function() {
                $('#datetime_local').val(moment().format('YYYY-MM-DDTHH:mm:ss.SSSZ'));
                $('#pan').payment('formatCardNumber');
                $('#expiry_month').payment('restrictNumeric');
                $('#expiry_year').payment('restrictNumeric');
                $('#cvc').payment('formatCardCVC');
            });


            this.connect(this.submitButton, "click", function (e) {
                // Only on mobile stop event bubbling!
                this._stopBubblingEventOnMobile(e);
                this._submitForm(e);

            });
        },


        _TokenMicroflow: function (self) {

            // Create a GingerPayments entity
            mx.data.create({
                entity: self.GingerPaymentsEntity,
                callback: function(GingerPaymentsObj) {
                    console.log('GUID',GingerPaymentsObj.getGuid());

                    GingerPaymentsObj.set(self.PanHashAttribute, self.pan_hash);
                    GingerPaymentsObj.set(self.TokenAttribute, self.vault_token);

                    console.log(GingerPaymentsObj);

                    // If a microflow has been set execute the microflow on a click.
                    if (self.saveToken_Microflow !== "") {
                        mx.data.action({
                            params: {
                                applyto: "selection",
                                actionname: self.saveToken_Microflow,
                                guids: [ GingerPaymentsObj.getGuid() ]
                            },
                            store: {
                                caller: self.mxform
                            },
                            callback: function (obj) {
                                //TODO what to do when all is ok!

                            },
                            error: dojoLang.hitch(self, function (error) {
                                logger.error(self.id + ": An error occurred while executing microflow: " + error.description);
                            })
                        }, this);
                    }

                },
                error: function(e) {
                    console.log("an error occured: " + e);
                }
            });

        },

        // Rerender the interface.
        _updateRendering: function (callback) {
            logger.debug(this.id + "._updateRendering");
            //this.colorSelectNode.disabled = this._readOnly;
            //this.colorInputNode.disabled = this._readOnly;

            if (this._contextObj !== null) {
                dojoStyle.set(this.domNode, "display", "block");

                //var colorValue = this._contextObj.get(this.backgroundColor);

                //this.colorInputNode.value = colorValue;
                //this.colorSelectNode.value = colorValue;

                //dojoHtml.set(this.infoTextNode, this.messageString);
                //dojoStyle.set(this.infoTextNode, "background-color", colorValue);
            } else {
                dojoStyle.set(this.domNode, "display", "none");
            }

            // Important to clear all validations!
            this._clearValidations();

            // The callback, coming from update, needs to be executed, to let the page know it finished rendering
            this._executeCallback(callback);
        },

        // Handle validations.
        _handleValidation: function (validations) {
            logger.debug(this.id + "._handleValidation");
            this._clearValidations();
            /*
            //var validation = validations[0], message = validation.getReasonByAttribute(this.backgroundColor);

            if (this._readOnly) {
                //validation.removeAttribute(this.backgroundColor);
            } else if (message) {
                this._addValidation(message);
                //validation.removeAttribute(this.backgroundColor);
            }
            */
        },



        // Clear validations.
        _clearValidations: function () {
            logger.debug(this.id + "._clearValidations");
            dojoConstruct.destroy(this._alertDiv);
            this._alertDiv = null;
        },


        // Show an error message.
        _showError: function (message) {
            logger.debug(this.id + "._showError");
            if (this._alertDiv !== null) {
                dojoHtml.set(this._alertDiv, message);
                return true;
            }
            this._alertDiv = dojoConstruct.create("div", {
                "class": "alert alert-danger",
                "innerHTML": message
            });
            dojoConstruct.place(this._alertDiv, this.domNode);
        },

        // Add a validation.
        _addValidation: function (message) {
            logger.debug(this.id + "._addValidation");
            this._showError(message);
        },

        _unsubscribe: function () {
            if (this._handles) {
                dojoArray.forEach(this._handles, function (handle) {
                    this.unsubscribe(handle);
                });
                this._handles = [];
            }
        },

        // Reset subscriptions.
        _resetSubscriptions: function () {
            logger.debug(this.id + "._resetSubscriptions");
            // Release handles on previous object, if any.
            this._unsubscribe();

            // When a mendix object exists create subscribtions.
            // We dont need this
            /*
             if (this._contextObj) {
             var objectHandle = this.subscribe({
             guid: this._contextObj.getGuid(),
             callback: dojoLang.hitch(this, function (guid) {
             this._updateRendering();
             })
             });

             var attrHandle = this.subscribe({
             guid: this._contextObj.getGuid(),
             attr: this.backgroundColor,
             callback: dojoLang.hitch(this, function (guid, attr, attrValue) {
             this._updateRendering();
             })
             });

             var validationHandle = this.subscribe({
             guid: this._contextObj.getGuid(),
             val: true,
             callback: dojoLang.hitch(this, this._handleValidation)
             });

             this._handles = [ objectHandle, attrHandle, validationHandle ];
             }*/
        },

        _executeCallback: function (cb) {
            if (cb && typeof cb === "function") {
                cb();
            }
        }
    });
});

require(["AirMilesMastercard/widget/AirMilesMastercard"]);
